//------------------------------------------------------------------------------
//  此代码版权（除特别声明或在RRQMCore.XREF命名空间的代码）归作者本人若汝棋茗所有
//  源代码使用协议遵循本仓库的开源协议及附加协议，若本仓库没有设置，则按MIT开源协议授权
//  CSDN博客：https://blog.csdn.net/qq_40374647
//  哔哩哔哩视频：https://space.bilibili.com/94253567
//  Gitee源代码仓库：https://gitee.com/RRQM_Home
//  Github源代码仓库：https://github.com/RRQM
//  交流QQ群：234762506
//  感谢您的下载和使用
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
using System;
using System.Windows;
using System.Windows.Media.Animation;

namespace MyMessageDialog
{
    /// <summary>
    /// DialogWindow.xaml 的交互逻辑
    /// </summary>
    public partial class DialogWindow : Window
    {
        public DialogWindow(string mes)
        {
            InitializeComponent();
            this.Tb_Content.Text = mes;
            this.Loaded += this.DialogWindow_Loaded;
        }

        private void DialogWindow_Loaded(object sender, RoutedEventArgs e)
        {
            DoubleAnimation animation_Top = new DoubleAnimation();
            animation_Top.To = this.Owner.Top;
            animation_Top.BeginTime = TimeSpan.FromSeconds(2);
            animation_Top.Duration = TimeSpan.FromSeconds(1);
            animation_Top.Completed += this.Animation_Top_Completed;
            this.BeginAnimation(Window.TopProperty, animation_Top);

            DoubleAnimation animation_Op = new DoubleAnimation();
            animation_Op.To = 0;
            animation_Op.BeginTime = TimeSpan.FromSeconds(2);
            animation_Op.Duration = TimeSpan.FromSeconds(1);
            this.BeginAnimation(Window.OpacityProperty, animation_Op);
        }

        private void Animation_Top_Completed(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}